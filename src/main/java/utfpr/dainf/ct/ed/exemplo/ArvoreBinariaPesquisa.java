 package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
import java.util.ArrayDeque;
import java.util.Stack;

public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {
    protected ArvoreBinariaPesquisa<E> pai;
    protected ArvoreBinariaPesquisa<E> esquerda;
    protected ArvoreBinariaPesquisa<E> direita;
    
    
    // para percurso iterativo
    public boolean inicio=true;
    private Stack<ArvoreBinariaPesquisa<E>> pilha;
    private ArrayDeque<ArvoreBinariaPesquisa<E>> fila;
    private ArvoreBinariaPesquisa<E> ultimoVisitado;
    private ArvoreBinariaPesquisa<E> noPos;
    private int contador;
    
    
     protected void visita(ArvoreBinariaPesquisa<E> no) {
        System.out.print(" " + no.valor);
    }
    public void visitaEmOrdem(ArvoreBinariaPesquisa<E> raiz) {
        if (raiz != null) {
            visitaEmOrdem(raiz.esquerda);
            visita(raiz);
            visitaEmOrdem(raiz.direita);
        }
    }
    public void visitaEmOrdem() {
        visitaEmOrdem(this);
    }
    
    private void inicializaPilha() {
        if (pilha == null) {
            pilha = new Stack<>();
        }
    }
    
    private void inicializaFila() {
        if (fila == null) {
            fila = new ArrayDeque<>();
        }
    }
    public void reinicia() {
        inicializaPilha();
        inicializaFila();
        pilha.clear();
        fila.clear();
        ultimoVisitado = this;
        inicio=true;
    }
    public ArvoreBinariaPesquisa<E> proximoEmOrdem() {
        ArvoreBinariaPesquisa<E> resultado = null;
        if (inicio) {
            reinicia();
            inicio = false;
        }
        if (!pilha.isEmpty() || ultimoVisitado != null) {
            while (ultimoVisitado != null) {
                pilha.push(ultimoVisitado);
                ultimoVisitado = ultimoVisitado.esquerda;
            }
            ultimoVisitado = pilha.pop();
            resultado = ultimoVisitado;
            ultimoVisitado = ultimoVisitado.direita;
        }
        return resultado;
    }
    
    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     * @param pai O nó pai.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó pai deste nó.
     * @return O nó pai.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    //--------------------------------------------------
    /**
     * Insere uma subárvore à esquerda deste nó.
     * A subárvore à esquerda deste nó é inserida na folha mais à esquerda
     * da subárvore inserida.
     * @param a A subárvore a ser inserida.
     * @return A subárvore inserida.
     */
    public ArvoreBinariaPesquisa<E> insereEsquerda(ArvoreBinariaPesquisa<E> a) {
        ArvoreBinariaPesquisa<E> e = esquerda;
        ArvoreBinariaPesquisa<E> x = a;
        esquerda = a;
        while (x.esquerda != null)
            x = x.esquerda;
        x.esquerda = e;
        return a;
    }
    
    /**
     * Insere uma subárvore à direita deste nó.
     * A subárvore à direita deste nó é inserida na folha mais à direita
     * da subárvore inserida.
     * @param a A subárvore a ser inserida.
     * @return A subárvore inserida.
     */
    public ArvoreBinariaPesquisa<E> insereDireita(ArvoreBinariaPesquisa<E> a) {
        ArvoreBinariaPesquisa<E> d = direita;
        ArvoreBinariaPesquisa<E> x = a;
        direita = a;
        while (x.direita != null)
            x = x.direita;
        x.direita = d;
        return a;
    }
    
    //-----------------------------------------------------
    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
        ArvoreBinariaPesquisa<E>resultado= new ArvoreBinariaPesquisa<>();
        ArvoreBinariaPesquisa<E> raiz=this;
        ArvoreBinariaPesquisa<E>aux= new ArvoreBinariaPesquisa<>();
        aux=raiz;
        while(valor!=null && aux!=null){
            if(valor==aux.valor)
            {   
                resultado=aux;
                return resultado;
            }
                
            else{
                if(valor.compareTo(aux.valor)>0)
                {
                    aux=aux.direita;
                    //pesquisa(valor);   
                } 
                else {
                    aux=aux.esquerda;
                    //pesquisa(valor);
                }
            }
        }
        return null;
        //throw new RuntimeException("Não implementado");
    }

    /**
     * Retorna o nó da árvore com o menor valor.
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo() {
        ArvoreBinariaPesquisa<E>resultado= new ArvoreBinariaPesquisa<>();
        ArvoreBinariaPesquisa<E> raiz=this;
        ArvoreBinariaPesquisa<E>aux= new ArvoreBinariaPesquisa<>();
        if(raiz!=null)
        {
            resultado=raiz;
            aux=raiz;
            while(aux!=null)
            {
                resultado=aux;
                aux=aux.esquerda;                
            }
        }
        return resultado;
    }

    /**
     * Retorna o nó da árvore com o maior valor.
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo() {
        ArvoreBinariaPesquisa<E>resultado= new ArvoreBinariaPesquisa<>();
        ArvoreBinariaPesquisa<E> raiz=this;       
        ArvoreBinariaPesquisa<E>aux= new ArvoreBinariaPesquisa<>();
        if(raiz!=null)
        {
            resultado=raiz;
            aux=raiz;
            while(aux!=null)
            {
                resultado=aux;
                aux=aux.direita;                
            }
        }
        return resultado;
    }

    /**
     * Retorna o nó sucessor do nó especificado.
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {@null}.
     */
    
    
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E> resultado= new ArvoreBinariaPesquisa<>();
        ArvoreBinariaPesquisa<E> aux=this;
        if(no!=null){
            resultado=aux;
            while(aux!=null)
            {
                if(no.valor.compareTo(aux.valor)>=0) //vejo se é maior primeiro
                {
                    if(no.valor.compareTo(resultado.valor)>=0)
                    {
                        resultado=aux.direita;
                    }
                    aux=aux.direita;
                  
                }
                else
                {
                    resultado=aux;
                    aux=aux.esquerda;
                }
            }
            return resultado;
        }
        return null;
    }
    
    

    /**
     * Retorna o nó predecessor do nó especificado.
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E> resultado= new ArvoreBinariaPesquisa<>();
        ArvoreBinariaPesquisa<E> aux=this;
        if(no!=null){
            resultado=aux;
            while(aux!=null)
            {
                if(no.valor.compareTo(aux.valor)<=0) //vejo se é menor primeiro
                {
                    if(no.valor.compareTo(resultado.valor)<=0)
                    {
                        resultado=aux.esquerda;
                    }
                    aux=aux.esquerda;
                  
                }
                else
                {
                    resultado=aux;
                    aux=aux.direita;
                }
            }
            return resultado;
        }
        return null;
    }

    /**
     * Insere um nó contendo o valor especificado na árvore.
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor) {
        ArvoreBinariaPesquisa<E>resultado= new ArvoreBinariaPesquisa<>();
        ArvoreBinariaPesquisa<E> raiz=this;
        ArvoreBinariaPesquisa<E> noPos=null;
        ArvoreBinariaPesquisa<E>aux= new ArvoreBinariaPesquisa<>();
        if(valor!=null)
        {
            if(raiz.valor==null){
                raiz.valor=valor;
            }
            else
            {
                aux=raiz;
                while(aux!=null)
                {
                    noPos=aux;
                    if(valor.compareTo(aux.valor)>0)
                    {
                        aux=aux.direita;
                    }
                    else if(valor.compareTo(aux.valor)<0)
                    {
                        aux=aux.esquerda;
                    }
                    else
                        aux=null;
                }
                resultado.valor=valor;
                if(valor.compareTo(noPos.valor)>0){
                    noPos.insereDireita(resultado);
                    resultado.pai=noPos;
                }
                else if(valor.compareTo(noPos.valor)<0){
                    noPos.insereEsquerda(resultado);
                    resultado.pai=noPos;
                }
                
            }
            return resultado;
        }
        return null;
    }

    /**
     * Exclui o nó especificado da árvore.
     * Se a raiz for excluída, retorna a nova raiz.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E>resultado= new ArvoreBinariaPesquisa<>();
        ArvoreBinariaPesquisa<E> raiz=this;
        ArvoreBinariaPesquisa<E> noPos;
        ArvoreBinariaPesquisa<E>noat= new ArvoreBinariaPesquisa<>(); //no atual
        noat=raiz;
        if(no!=null)
        {
            if(no.valor.compareTo(raiz.valor)==0 && raiz.direita==null && raiz.esquerda==null)//somente raiz
                raiz.valor=null;
            else
            {
                while(noat!=null)
                {
                    if(no.valor.compareTo(noat.valor)==0) //encontrou o nó a ser removido
                    {
                        if(noat.direita==null && noat.esquerda==null) //nó sem filhos
                        {
                            if(noat.valor.compareTo(noat.pai.valor)>0) //se o no ta na direita do pai
                            {
                                noat.pai.direita=null;
                                noat=null;
                            }
                            else 
                            {
                                noat.pai.esquerda=null;
                                noat=null;
                            }
                        }
                        else if(noat.direita!=null && noat.esquerda!=null) //se o nó tem dois filhos
                        {
                            noPos=sucessor(no); //pega o sucessor do no
                            if(noPos.direita==null && noPos.esquerda==null) //sucessor sem filhos
                            {
                                noat.valor=noPos.valor;
                                if(noPos.valor.compareTo(noPos.pai.valor)>0) //se o no ta na direita do pai
                                    noPos.pai.direita=null;
                                else 
                                    noPos.pai.esquerda=null;
                            }
                            else 
                            {
                                noat.valor=noPos.valor;
                                if(noPos.direita!=null) //sucessor com filho direito
                                {
                                    noat.direita=noPos.direita;
                                    noPos.direita.pai=noat;
                                }
                                else                    //sucessor com filho esquerdo
                                {
                                    noat.direita=noPos.esquerda;
                                    noPos.esquerda.pai=noat;
                                }



                            }
                            break;
                        }
                        else //se o nó tem um filho
                        {  
                            if(noat.direita!=null) //filho na direita
                            {
                                if(noat.pai.direita.valor.compareTo(noat.valor)==0)
                                    noat.pai.direita=noat.direita;
                                else
                                    noat.pai.esquerda=noat.direita;

                                noat.direita.pai=noat.pai;
                            }
                            else //filho na esquerda
                            {
                                if(noat.pai.direita.valor.compareTo(noat.valor)==0)
                                    noat.pai.direita=noat.esquerda;
                                else
                                   noat.pai.esquerda=noat.esquerda;

                                noat.esquerda.pai=noat.pai;
                            }
                            noat=null;
                        }

                    }
                    else if(no.valor.compareTo(noat.valor)>0) //se n achou vai pra direita
                    {
                        noat=noat.direita;
                    }
                    else  //se n achou vai pra esquerda
                    {
                        noat=noat.esquerda;
                    }
                }
            }
        }
        return null;   
    }
    
}



